import express, { Application, Request, Response } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';
import morgan from "morgan";
import helmet from "helmet";
import wash from "./routes/washapi";
import {verifyToken} from './middlewares/verifyToken'
const app: Application = express();
const PORT: number = 8000;
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(morgan("common"));
app.use(helmet());
app.use(cors());

app.use('/v1/',verifyToken,wash);

app.get('/', (req: Request, res: Response) => {
    res.send('Express + TypeScript Server');
});

app.listen(PORT, () => {
    console.log(`Server is running at http://localhost:${PORT}`);
});