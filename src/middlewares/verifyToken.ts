import { NextFunction,Request, Response } from 'express';
export const verifyToken = (req: Request, res: Response, next: NextFunction) => {

    //reading the headers
    const token = req.headers['auth-token'];
    if (token != 'apitoken'){
        return res.status(403).json({message: 'auth-token missing'})
    }
    next();

}
